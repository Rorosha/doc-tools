Welcome to the Taskotron setup tutorial! This guide will walk you through the steps to set up a local instance of `Taskotron <{{d['facts.txt|idio']['homepage']}}>`_. The current version is {{d['facts.txt|idio']['version']}}. 

Things you'll need
------------------

A host (H) capable of running two Fedora 19 VMs (i386 or x86_64), a Master (M) and a Slave (S). 

::

  {{ d['facts.txt|idio|t']['vm-specs'] }}

For both VMs, do WebServer installation and accept all defaults for partitioning. We don't need anything fancy for our purposes.

Set up the Host
---------------

While your VMs are finishing up their installations, we can set up H.  We'll be working out of /srv for most of this tutorial and working with a tool called `Ansible <http://docs.ansible.com/index.html>`_. First, install Ansible:

::

  yum install ansible

Second, create the directory tree we'll be working out of.

::

  {{ d['host-vm-setup.txt|idio']['make-host-dirs'] }}

Next, the ansible playbooks we'll be using need to be cloned from bitbucket.

::

  {{ d['host-vm-setup.txt|idio']['clone-playbooks'] }}

Hopefully by now your VMs are done or almost done installing. Once they are, we need to set up ssh via keys on both machines. For this tutorial, we'll keep all the keys we use in /srv/keys. 

::

  {{ d['host-vm-setup.txt|idio']['handle-ssh-keys'] }}

Later on we'll also be needing two other non-user keys for gitolite and S authenticating to M - so we'll go ahead and create those now.

::

  {{ d['host-vm-setup.txt|idio']['handle-non-user-keys'] }}

Now that we're done with the ssh keys we'll be needing, it's time to get our SSL certs. Log into M and install mod_ssl and copy the generated cert to H.

::

  {{ d['host-vm-setup.txt|idio']['install-mod_ssl'] }}

And copy the keys to H.

::

  {{ d['host-vm-setup.txt|idio']['copy-ssl-certs'] }}

Running the Master Playbook
---------------------------

Now that your host is set up and ready to go, we're going to configure ansible to auto configure M and S. Example settings were included in the 'ansible-playbooks' repo we cloned to /srv/ansible/qa.

::

  {{ d['setup-ansible.txt|idio']['copy-settings'] }}

Now that we have a template to work with, we're going to edit some settings and create an inventory file for ansible. Edit taskotron-local.yml file with new keys and options (assuming you kept the normal file names).

::

  {{ d['setup-ansible.txt|idio']['edit-settings'] }}

Once those edits are done, create a file named "local" in the /srv/ansible/qa and make it look like this:

::

  {{ d['setup-ansible.txt|idio']['create-inventory'] }}

This file is the inventory file for ansible - ansible looks here to know which machines it's allowed to connect to.

Now we're ready to run the first playbook task on M.

::

  {{ d['run-master-playbooks.txt|idio']['run-t-base'] }}

This command resets the root password and configures a second user for passwordless sudo. The created user is 'qaadmin' that you set in your /srv/ansible/private/qa/taskotron-local.yml file on H.

Once that's complete, run the full playbook on M. 

::

  {{ d['run-master-playbooks.txt|idio']['run-full'] }}

The base task (previous step denoted by '-t base' at the end of the command) created your qaadmin user, so now qaadmin will be the user we specify in ansible calls. Note the change from '-u <M username>' to '-u qaadmin' as well as the addition of --private-key arguments. This is the same key you created and moved with ssh-copy-id earlier.

On H, open a browser and navigate to the M IP and you'll see a landing page. Nothing is configured at this point - but the foundation for M is done.

Configuring Repos
-----------------

On H, open your .ssh/config file and add:

::

  {{ d['configure-repos.txt|idio|t']['ssh-config'] }}

Somewhere outside of /srv (like your home directory), clone the gitolite admin repo from M.

::

  {{ d['configure-repos.txt']['clone-gitolite'] }}

Once in this directory, you'll find 'conf/' and 'keydir/' directories. Copy your id_buildslave along with your git key.

::

  {{ d['configure-repos.txt|idio|t']['copy-keys'] }}

The last step for configuring gitolite is to edit conf/gitolite.conf to look like this:

::

  repo gitolite-admin
    RW+     =   taskgit-admin
    RW+	    =   username
  repo testing
    RW+     =   @all
  repo rpmlint
    RW+	    =   @all

Once those changes are made to the repo, add, commit and push back to M.

::

  {{ d['configure-repos.txt|idio|t']['git-actions'] }}

Now we need a task for Taskotron to run. The FedoraQA bitbucket account has a task for rpmlint. We're going to clone this, rename it to rpmlint and push that to M. You already set up the gitolite conf with the final section "repo rpmlint." You should do this in the same area you cloned gitolite-admin (outside of /srv).

::

  {{ d['configure-repos.txt|idio|t']['mirror-rpmlint'] }}

Now the master is configured and ready to have slaves attached to it! 

Running the Slave Playbook
--------------------------

Just like with M, you're going to run the '-t base' task first as the slave user in order to run the initial setup.

::

  {{ d['run-slave-playbooks.txt|idio|t']['run-t-base'] }}

SSH into S and edit /etc/hosts to see M from S

::

  {{ d['run-slave-playbooks.txt|idio|t']['edit-hosts'] }}

Now run the complete playbook for S.

::

  {{ d['run-slave-playbooks.txt|idio|t']['run-full'] }}

The last step before trying some builds is to do the initial clone from M to add M to the .ssh/known_hosts on S.

::

  {{ d['run-slave-playbooks.txt|idio|t']['update-known_hosts'] }}

Trigger Builds
--------------

Now S should be attached to M. Go back to the web interface for M.

Open http://M-ip/taskmaster and login with the username and password you specified in taskotron-local.yml

Navigate to http://M-ip/taskmaster/builders/statictasks

In the rpmcheck field group, fill out the last three inputs.

  name of check to run: rpmlint

  envr of package to test: simple-xml-2.7.1-2.fc20 #just grab something off koji

  arch of rpm to test: noarch

Hopefully the build ran and was reported as a success.
[Note: Add section about needing to check the stdio results to see what actually happened.]

Now that you can manually trigger builds, it's time to set up the trigger on M to listen to fedmsg for pushes.

SSH into M, edit /etc/hosts to reflect M's hostname and start the trigger:

::

  {{ d['trigger-builds.txt|idio|t']['edit-hosts'] }}

And finally, start the trigger!

::

  {{ d['trigger-builds.txt|idio|t']['start-trigger'] }}

Congrats, now you have a working dev instance of Taskotron! Now your master is listening for messages from Koji. When a build is pushed, your M will task S to run the rpmlint test on the recently pushed package.
