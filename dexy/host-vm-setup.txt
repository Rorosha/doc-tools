### @export "make-host-dirs"

mkdir -p /srv/{keys,ansible/private/qa/certs/taskotron-local}
cd /srv/ansible/ 

### @export "enable-vm-ssh"

systemctl start sshd.service

### @export "clone-playbooks"

git clone https://bitbucket.org/fedoraqa/ansible-playbooks.git qa

### @export "handle-ssh-keys"

cd /srv/keys
ssh-keygen -f vm-user-key
ssh-keygen -f user_name.git
ssh-copy-id -i vm-user-key root@slave-ip
ssh-copy-id -i vm-user-key root@master-ip

### @export "handle-non-user-keys"

cd /srv/ansible/private/qa/certs/taskotron-local
ssh-keygen -f taskgit-admin
ssh-keygen -f id_buildslave

### @export "install-mod_ssl"

yum install mod_ssl

### @export "copy-ssl-certs"

cd /srv/ansible/private/qa/certs/taskotron-local
scp -i /srv/keys/vm-user-key root@master-ip:/etc/pki/tls/certs/localhost.crt .
scp -i /srv/keys/vm-user-key root@master-ip:/etc/pki/tls/private/localhost.key .
