This is the source for the initial taskotron setup tutorials. There are 
two formats of the tutorial, Dexy[1] and Sphinx[2]. After installing 
the packages from requirements.txt, setup is simple.

From the /dexy directory, run `dexy setup` to initialize dexy and then run 
`dexy` to build the document. 

For the sphinx doc, cd into the sphinx directory and run `make html`. 

Dexy will create output in the output-site directory and sphinx will create 
output in the _build/html directory. 

[1] http://dexy.it/
[2] http://sphinx-doc.org/index.html